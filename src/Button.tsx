interface Props {
  children: React.ReactNode;
  onClick?: () => void;
  className?: string;
}

const Button = ({ children, ...props }: Props) => {
  return <button {...props}>{children}</button>;
};

export default Button;

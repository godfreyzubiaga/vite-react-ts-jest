import { render, fireEvent } from "@testing-library/react";
import Button from "./Button";

describe("Button", () => {
  it("renders children", () => {
    const { getByText } = render(<Button>Hello</Button>);
    expect(getByText("Hello")).toBeInTheDocument();
  });

  it("calls onClick when clicked", () => {
    const handleClick = jest.fn();
    const { getByRole } = render(
      <Button onClick={handleClick}>Click me</Button>
    );
    fireEvent.click(getByRole("button"));
    expect(handleClick).toHaveBeenCalled();
  });

  it("applies className", () => {
    const { getByRole } = render(<Button className="my-class">Button</Button>);
    expect(getByRole("button")).toHaveClass("my-class");
  });
});

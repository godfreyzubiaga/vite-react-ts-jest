import { render, fireEvent } from "@testing-library/react";
import App from "./App";

describe("App", () => {
  test("renders the App component", () => {
    const { getByText } = render(<App />);
    const linkElement = getByText(/Vite \+ React/i);
    expect(linkElement).toBeInTheDocument();
  });

  test("increments the count when the button is clicked", () => {
    const { getByText } = render(<App />);
    const buttonElement = getByText(/count is 0/i);
    fireEvent.click(buttonElement);
    expect(buttonElement).toHaveTextContent(/count is 1/i);
  });

  test("displays the correct initial count", () => {
    const { getByText } = render(<App />);
    const buttonElement = getByText(/count is 0/i);
    expect(buttonElement).toBeInTheDocument();
  });

  test("displays the correct logos", () => {
    const { getByAltText } = render(<App />);
    const viteLogoElement = getByAltText(/Vite logo/i);
    const reactLogoElement = getByAltText(/React logo/i);
    expect(viteLogoElement).toBeInTheDocument();
    expect(reactLogoElement).toBeInTheDocument();
  });
});
